package in.forsk.intentexpriment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class SecondActivity extends AppCompatActivity {

    Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Bundle extras = getIntent().getExtras();
        String intent_string = "";
        if (extras != null) {
            intent_string = getIntent().getStringExtra("BtnName");
        }

        btn = (Button)findViewById(R.id.button6);

        btn.setText(intent_string);


    }

    public void onClickSecondActivity(View v){
        Intent resultIntent = new Intent();
        resultIntent.putExtra("result", "Text From Activity 2");
        setResult(RESULT_OK, resultIntent);
        finish();
    }
}
